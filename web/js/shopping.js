$('.update-cart').on('click', (evt) => {
	evt.preventDefault();
	let q = document.querySelector(`#q${evt.target.dataset.id}`).value;

	let csrfParam = document.querySelector('[name="csrf-param"]').content;
	let csrfToken = document.querySelector('[name="csrf-token"]').content;

	let data = new URLSearchParams;
	data.append('items[id]', evt.target.dataset.id);
	data.append('items[quantity]', q || 1);
	data.append(csrfParam, csrfToken);

	let path = location.pathname.split('/');
	path.splice(2, 1, 'cart-add');

	let request = fetch(location.origin + path.join('/'), {
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		credentials: 'include',
		body: data
	});

	request.then((response) => {
		console.log(response)
	});
});

$('.quantity').on('blur', (evt) => {
	if(evt.target.value === '') {
		evt.target.value = 1;
	}
});