$('.update-details').on('click', (evt) => {
	evt.preventDefault();

	let csrfParam = document.querySelector('[name="csrf-param"]').content;
	let csrfToken = document.querySelector('[name="csrf-token"]').content;

	let data = new URLSearchParams;
	for(let field of document.querySelector('#shipping')) {
		data.append(field.name, field.value);
	}
	// data.append('items[id]', evt.target.dataset.id);
	// data.append('items[quantity]', q || 1);

	let request = fetch(location.href, {
		method: 'POST',
		body: data,
	});

	request.then((response) => response.json())
		.then(updater);
});

function updater(json) {
	let rate = Number(json.rates[0].amount);

	let shippingPrice = document.querySelector('#shipping_price');
	shippingPrice.innerHTML = `$${rate}`;

	let total = document.querySelector('#total');
	let initialTotal = Number(total.dataset.initialTotal);

	document.querySelector('#total_amount').innerHTML = `$${Math.fround(initialTotal + rate).toFixed(2)}`;

	let frag = new DocumentFragment();
	let strong = document.createElement('strong');
	strong.textContent = 'Ship to: ';
	let name = document.createElement('div');
	name.textContent = json.address_to.name;
	let address = document.createElement('div');
	address.textContent = `${json.address_to.street1}, ${json.address_to.city}, ${json.address_to.state}, ${json.address_to.zip}`;
	let city = document.createElement('div');
	let els = [strong, name, address];

	for(let el of els) {
		frag.appendChild(el);
	}

	document.querySelector('#shipping_details').appendChild(frag);
}

