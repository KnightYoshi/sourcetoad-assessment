$('.remove-item').on('click', function(evt) {
	evt.preventDefault();

	let csrfParam = document.querySelector('[name="csrf-param"]').content;
	let csrfToken = document.querySelector('[name="csrf-token"]').content;

	let data = new URLSearchParams;
	data.append('itemid', this.dataset.id);
	data.append(csrfParam, csrfToken);

	let path = location.pathname.split('/');
	path.splice(2, 1, 'cart-remove');

	let request = fetch(location.origin + path.join('/'), {
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		credentials: 'include',
		body: data
	});

	request.then((response) => {
		location.reload();
	});
});


$('.update-item').on('click', (evt) => {
	evt.preventDefault();
	let q = document.querySelector(`#q${evt.target.dataset.id}`).value;

	let csrfParam = document.querySelector('[name="csrf-param"]').content;
	let csrfToken = document.querySelector('[name="csrf-token"]').content;

	let data = new URLSearchParams;
	data.append('itemid', evt.target.dataset.id);
	data.append('quantity', q || 1);
	data.append(csrfParam, csrfToken);

	let path = location.pathname.split('/');
	path.splice(2, 1, 'cart-update');

	let request = fetch(location.origin + path.join('/'), {
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		credentials: 'include',
		body: data
	});

	request.then((response) => {
		location.reload();
	});
});