<?php

$data = require(__DIR__ . '/data/array1.php');

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'array1' => $data,
    'taxRate' => 7,
    'shippoToken' => 'shippo_test_101e85bb6883ad2abd792e979c623db564a67fdc',
    'address' => [
        'name' => 'Sourcetoad',
        'street1' => '2901 W Busch Blvd #1018',
        'city' => 'Tampa',
        'state' => 'FL',
        'zip' => '33618',
        'country' => 'US'
    ]
];
