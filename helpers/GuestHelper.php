<?php

namespace app\helpers;

class GuestHelper
{
    /**
     * @param $array the array to display
     * @param bool $isNested whether the array is nested (recursion)
     * @return string the guest display output
     */
    static public function displayGuest($array, $isNested = false) {

        $output = '';

        foreach($array as $key => $value) {

            $output .= '<div class="col-md-12">';
            if(!$isNested) {
                $output .= '<div><strong>Guest</strong></div>';
            }
            if(is_array($value)) {
                if(is_string($key)) {
                    $output .= "<strong>" . str_replace("_", " ", $key) . "</strong>";
                }
                $output .= self::displayGuest($value, true);
            }
            else {
                $output .=  str_replace("_", " ", $key) . ': ' . (!is_bool($value) ? $value : ($value === true ? 'Yes' : 'No'));
            }

            if(!$isNested && $key != array_key_last($array)) {
                $output .= '<hr class="col-md-12">';
            }

            $output .= "</div>";
        }

        return $output;
    }
}