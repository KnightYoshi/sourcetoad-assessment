<?php

/*
 * @var $data array
 */

use yii\widgets\ActiveForm;

function sorter($array, $orderby) {

    $arrayb = [];
    foreach($array as $key => $val) {
        if(isset($val[$orderby])) {
            $arrayb[] = $val[$orderby];
        }
        else if(is_array($val)) {
            print_r($key . '<br>');
            $arrayb[$key] = sorter($val, $orderby);
        }

    }

    asort($arrayb);

    $sorted = [];
    foreach($arrayb as $key => $val) {
        $sorted[] = $array[$key];
    }

    return $sorted;
}
$orderBy = Yii::$app->request->get('sort', 'guest_id');
if($orderBy === '') {
    $orderBy = 'guest_id';
}
?>
<form method="GET">
    <div class="form-group">
        <label class="control-label" for="customer-lastname">Order By (field name)</label>
        <input class="form-control" name="sort" type="text" value="<?= $orderBy ?>">
    </div>
    <button class="btn btn-primary" type="submit">Go!</button>
</form>
<div>
    <?= \app\helpers\GuestHelper::displayGuest(sorter($data, $orderBy)); ?>
</div>
