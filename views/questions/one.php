<?php

/*
 * @var $data array
 */

use app\helpers\GuestHelper;
?>

<h1>Guess Information</h1>
<div class="row">
    <?= GuestHelper::displayGuest($data, false); ?>
</div>