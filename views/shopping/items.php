<?php

/**
 * @var $this \yii\web\View
 * @var $cart \app\models\Cart
 * @var $items array
 */

$this->registerJsFile('/js/shopping.js');
?>

<div class="row">
    <?php foreach($items as $key => $item): ?>
    <div class="card" style="width: 18rem; margin:10px">
        <img class="card-img" src="https://via.placeholder.com/150x200?text=<?= $item['name'] ?>">
        <div class="card-body">
            <h5 class="card-title"><?= $item['name'] ?></h5>
            <p class="card-text"?>$<?= $item['price'] ?></p>
            <div class="form-group">Quantity:
                <input class="form-control quantity" id="q<?= $key ?>" type="number" value="1" min="1" max="99">
            </div>
            <button class="btn btn-primary update-cart" type="button" data-id="<?= $key ?>">Add to Cart</button>
        </div>
    </div>
    <?php endforeach; ?>
</div>