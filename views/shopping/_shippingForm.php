<?php

/**
 * @var $shippingForm \app\models\ShippingForm;
 */
?>

<?php $form = \yii\widgets\ActiveForm::begin(['id' => 'shipping']) ?>
<?= $form->field($shippingForm->getCustomer(), 'firstName')->textInput() ?>
<?= $form->field($shippingForm->getCustomer(), 'lastName')->textInput() ?>
<?= $form->field($shippingForm->getCustomer(), 'phoneNumber')->textInput() ?>
<?= $form->field($shippingForm->getCustomer(), 'street1')->textInput() ?>
<?= $form->field($shippingForm->getCustomer(), 'street2')->textInput() ?>
<div class="row">
    <div class="col-md-3">
        <?= $form->field($shippingForm->getCustomer(), 'city')->textInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($shippingForm->getCustomer(), 'state')->textInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($shippingForm->getCustomer(), 'zip')->textInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($shippingForm->getCustomer(), 'country')->textInput() ?>
    </div>
</div>
<hr>
<button class="btn btn-primary update-details" type="submit">Update Details</button>
<?php \yii\widgets\ActiveForm::end() ?>