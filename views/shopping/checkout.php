<?php

/**
 * @var $this \yii\web\View
 * @var $shippingForm \app\models\ShippingForm
 * @var $cart \app\models\Cart
 */

$this->registerJsFile('/js/checkout.js');
?>

<h3>Shipping information</h3>
<div class="row">
    <div class="col-md-6">
        <?= $this->render('_shippingForm', ['shippingForm'=> $shippingForm]) ?>
    </div>
    <div class="col-md-6">
        <div id="items">Items (<?= $cart->getItemCount() ?>): $<?= $cart->getSubTotal() ?></div>
        <div id="tax" data-initial-total="<?= $cart->getTotal() ?>">Tax: $<?= $cart->getSalesTax() ?></div>
        <div id="shipping" data-initial-total="<?= $cart->getTotal() ?>">Shipping:
            <span id="shipping_price"><small>Update Details</small></span></div>
        <hr>
        <div id="total" data-initial-total="<?= $cart->getTotal() ?>" data-initial-total="<?= $cart->getTotal() ?>">
            Total:
            <span id="total_amount">
                <small>Update Details</small>
            </span>
        </div>
        <div id="shipping_details"></div>
    </div>
</div>