<?php

/**
 * @var $this \yii\web\View
 * @var $cart \app\models\Cart
 */

use yii\helpers\Html;


$this->registerJsFile('/js/cart.js')
?>

<div>
    <?php foreach($cart->getItems() as $item): ?>
        <div class="card" style="margin-bottom: 1rem;">
            <div class="card-body">
                <div style="float: right">
                    <button class="btn btn-danger remove-item" type="button" data-id="<?= $item->getId() ?>">
                        <i class="fas fa-trash"></i>
                    </button>
                </div>
                <h5 class="card-title">
                    <?= $item->getName(); ?>
                    <small>x <?= $item->getQuantity() ?></small>
                </h5>
                <div>Price: $<?= number_format($item->getPrice() * $item->getQuantity(), 2) ?></div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Quantity:</span>
                    </div>
                    <input id="q<?= $item->getId() ?>" class="form-control" type="number" min="1" value="<?= $item->getQuantity() ?>" style="flex:.2 1 0;min-width: 75px">
                    <div class="input-group-append">
                        <button class="btn btn-info update-item" data-id="<?= $item->getId() ?>">Update</button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <hr>
    Subtotal: $<?= number_format($cart->getSubTotal(), 2) ?>
    <br>
    Tax: $<?= number_format($cart->getSalesTax(), 2) ?>
    <br>
    Total: $<?= number_format($cart->getTotal(), 2) ?>
    <div class="text-right">
        <a class="btn btn-primary" href="/shopping/checkout">Proceed to Checkout</a>
    </div>
</div>