<?php


namespace app\controllers;


use app\models\Cart;
use app\models\CartForm;
use app\models\Customer;
use app\models\ShippingForm;
use http\Client;
use Shippo_Shipment;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;

class ShoppingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionItems() {
        return $this->render('items', ['items' => CartForm::$itemList]);
    }

    public function actionCart() {

        $model = new CartForm();

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())  && $model->save()) {
            Yii::$app->session->setFlash('message', 'Cart was successfully updated');
        }

        return $this->render('cart', [
            'formModel' => $model,
            'cart' => Cart::getCart(),
        ]);
    }

    public function actionCartAdd() {
        /* @var $cart Cart */
        $item = Yii::$app->request->post('items');

        $cartForm = new CartForm(['items' => [$item]]);
        if(Yii::$app->request->isPost && $cartForm->save(false)) {
            return true;
        }

        return false;
    }

    public function actionCartUpdate() {
        $cartForm = new CartForm();
        $cartForm->update(Yii::$app->request->post('itemid'), Yii::$app->request->post('quantity'));
    }

    public function actionCartRemove() {
        Cart::getCart()->removeItem(Yii::$app->request->post('itemid'));

        Yii::$app->session->set('cart', Cart::getCart());

        return true;
    }

    public function actionCheckout() {
        $shippingForm = new ShippingForm();
        $shippingForm->setCustomer(new Customer());

        if(Yii::$app->request->isPost) {
            $shippingForm->setAttributes(Yii::$app->request->post());

            // store customer details
            Yii::$app->session->set('customer', $shippingForm->getCustomer());

            \Shippo::setApiKey(Yii::$app->params['shippoToken']);
            $fromAddress = Yii::$app->params['address'];
            $toAddress = $shippingForm->getCustomer()->getShippingDetails();

            $parcel = [];
            foreach(Cart::getCart()->getItems() as $item) {
                $parcel[] = [
                    'length'=> '20',
                    'width'=> '12',
                    'height'=> '15',
                    'distance_unit'=> 'in',
                    'weight'=> '1',
                    'mass_unit'=> 'lb',
                ];
            }

            $shipment = Shippo_Shipment::create([
                'address_from'=> $fromAddress,
                'address_to'=> $toAddress,
                'parcels'=> $parcel,
                'async'=> false
            ]);

            return $shipment;
        }

        return $this->render('checkout', [
            'shippingForm' => $shippingForm,
            'cart' => Cart::getCart(),
        ]);
    }
}