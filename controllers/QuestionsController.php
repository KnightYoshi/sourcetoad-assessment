<?php


namespace app\controllers;

use app\models\CartForm;
use app\models\Customer;
use Yii;
use yii\web\Controller;

class QuestionsController extends Controller
{
    public function actionOne() {
        return $this->render('one', [
            'data' => \Yii::$app->params['array1'],
        ]);
    }

    public function actionTwo() {
        return $this->render('two', [
            'data' => \Yii::$app->params['array1'],
        ]);
    }

    public function actionThree() {
        return $this->render('three', ['formModel' => new CartForm()]);
    }
}