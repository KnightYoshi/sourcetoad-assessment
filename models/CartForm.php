<?php


namespace app\models;


use Yii;
use yii\base\Model;

class CartForm extends Model
{
    /* @var $items Item[] */
    private $items = [];

    /* @var $customer Customer */
    private $customer;

    // ideally these would be objects
    // probably returned from a database, but good enough ¯\_(ツ)_/¯
    public static $itemList = [
        [
            'id' => 1,
            'name' => "Cap'n Crunch",
            'price' => 2.56
        ],
        [
            'id' => 2,
            'name' => 'Fruity Pebbles',
            'price' => 5.98
        ],
        [
            'id' => 3,
            'name' => 'Cheerios',
            'price' => 3.53
        ],
        [
            'id' => 4,
            'name' => 'Life',
            'price' => 3.01
        ],
        [
            'id' => 5,
            'name' => 'Frosted Flakes',
            'price' => 3.64
        ],
    ];

    public function rules() {
        return [
            [['items'], 'required']
        ];
    }

    public function afterValidate()
    {

        if(!Model::validateMultiple($this->getModels())) {
            $this->addError(null);
        }

        parent::afterValidate();
    }

    public function save($validate) {
        if($validate && !$this->validate()) {
            return false;
        }

        $customer = new Customer($this->customer);

        /** @var $cart Cart */
        $cart = Cart::getCart();
        $cart->setItems($this->items);
        $cart->setCustomer($customer);

        // "save" the cart — in this case to the session
        \Yii::$app->session->set('cart', $cart);

        return true;
    }

    public function update($itemId, $quantity) {
        $cart = Cart::getCart();
        foreach($cart->getItems() as $item) {
            if($item->getId() === (int)$itemId) {
                $item->setQuantity($quantity);
            }
        }

        $this->save(false);
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     */
    public function setItems($items)
    {
        foreach($items as $item) {
            if($item instanceof Item) {
                $this->items[] = $item;
            }
            else {
                $itemData = self::$itemList[$item['id']];
                $itemData['quantity'] = $item['quantity'];
                $item = new Item($itemData);
                $this->items[] = $item;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        if($customer instanceof Customer) {
            $this->customer = $customer;
        }
        else if(is_array($customer)) {
            $this->customer->setAttributes($customer);
        }
    }

    public function getModels() {
        $models = [
            'customer' => $this->customer
        ];

        foreach($this->items as $id => $item) {
            $models['item.' . $id] = $item;
        }

        return $models;
    }

    public function getItemList() {
        return self::$itemList;
    }
}