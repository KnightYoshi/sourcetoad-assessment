<?php


namespace app\models;


use Yii;
use yii\base\Model;

class Customer extends Model
{
    private $first_name;

    private $last_name;

    private $phoneNumber;

    private $address = [
        'deliveryAddress' => [
            'street1' => '',
            'street2' => '',
            'city' => '',
            'state' => '',
            'zip' => '',
            'country' => 'US',
        ],
        'billingAddress' => [
            'street1' => '',
            'street2' => '',
            'city' => '',
            'state' => '',
            'zip' => '',
            'country' => 'US',
        ],
    ];

    public function rules() {
        return [
            [['firstName', 'lastName', 'street1', 'street2', 'city', 'state', 'zip', 'country', 'phoneNumber'], 'string'],
//            [['address'], 'addressValidator']
        ];
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getStreet1() {
        return $this->address['deliveryAddress']['street1'];
    }

    public function setStreet1($street) {
        $this->address['deliveryAddress']['street1'] = $street;
    }

    public function getStreet2() {
        return $this->address['deliveryAddress']['street2'];
    }

    public function setStreet2($street) {
        $this->address['deliveryAddress']['street2'] = $street;;
    }

    public function getCity() {
        return $this->address['deliveryAddress']['city'];
    }

    public function setCity($city) {
        $this->address['deliveryAddress']['city'] = $city;
    }

    public function getState() {
        return $this->address['deliveryAddress']['state'];
    }

    public function setState($state) {
        $this->address['deliveryAddress']['state'] = $state;
    }

    public function getZip() {
        return $this->address['deliveryAddress']['zip'];
    }

    public function setZip($zip) {
        $this->address['deliveryAddress']['zip'] = $zip;
    }

    public function getCountry() {
        return $this->address['deliveryAddress']['country'];
    }

    public function setCountry($country) {
        $this->address['deliveryAddress']['country'] = $country;
    }

    /**
     * @return array
     */
    public function getShippingDetails() {
        return [
            'name' => "{$this->first_name} {$this->last_name}",
            'street1' => $this->getStreet1(),
            'street2' => $this->getStreet2(),
            'city' => $this->getCity(),
            'state' => $this->getState(),
            'zip' => $this->getZip(),
            'country' => $this->getCountry(),
            'phone' => $this->getPhoneNumber(),
        ];
    }

    private function addressValidator($attribute) {
        $goodData = true;
        foreach($this->$attribute as $key => $addressField) {
            if(is_array($addressField)) {
                $this->addressValidator($addressField);
            }
            else {
                if(gettype($addressField) !== 'string') {
                    $goodData = false;
                    $this->addError($attribute, "$key is invalid");
                }
            }
        }

        return $goodData;
    }
}