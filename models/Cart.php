<?php


namespace app\models;


use Yii;
use yii\base\Model;

class Cart extends Model
{
    private $customer;

    /**
     * @var Item[]
     */
    private $items = [];

    private $shippingPrice;

    private $total;

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        if($customer instanceof Customer) {
            $this->customer = $customer;
        }
        else {
            $customer = new Customer($customer);
            $this->customer = $customer;
        }
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        foreach($items as $item) {
            $this->addItem($item);
        }
    }

    public function addItem($item) {
        if(!($item instanceof Item)) {
            Throw new \InvalidArgumentException('Invalid item value, must be an instance of [[Item]]');
        }

        foreach($this->items as $existingItem) {
            if($existingItem->getId() === $item->getId()) {
                $quantity = $item->getQuantity();

                $item = $existingItem;
                $item->setQuantity($item->getQuantity() + $quantity);

                return;
            }
        }

        $this->items[] = $item;
    }

    public function updateCount($itemDetails) {
        foreach($this->items as $key => $item) {
            if($item->getId() === $itemDetails['id']) {
                $item->setQuantity($itemDetails['quantity']);
            }
        }
    }

    public function removeItem($itemId) {
        foreach($this->items as $key => $item) {
            if($item->getId() === (int)$itemId) {
                unset($this->items[$key]);
            }
        }
    }

    public function getSubTotal() {
        $total = 0;
        foreach($this->items as $item) {
            $total += $item->getPrice() * $item->getQuantity();
        }
        return round($total, 2);
    }

    public function getTotal() {
        $total = 0;
        foreach($this->items as $item) {
            $total += $item->getPrice() * $item->getQuantity();
        }
        return round((($this->getTaxRate() / 100) * $total) + $total, 2);
    }

    public function getSalesTax() {
        $total = 0;
        foreach($this->items as $item) {
            $total += $item->getPrice() * $item->getQuantity();
        }

        return round((($this->getTaxRate() / 100) * $total), 2);
    }

    public function getTaxRate() {
        return Yii::$app->params['taxRate'];
    }

    public function getItemCount() {
        $count = 0;
        foreach($this->items as $item) {
            $count += $item->getQuantity();
        }
        return $count;
    }

    /**
     * @return Cart
     */
    public static function getCart() {
        return \Yii::$app->session->get('cart', new self);
    }
}