<?php


namespace app\models;


use Yii;
use yii\base\Model;

class ShippingForm extends Model
{
    /**
     * @var Customer
     */
    private $customer;

    public function rules()
    {
        return [
            [['Customer'], 'required']
        ];
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        if($customer instanceof Customer) {
            $this->customer = $customer;
        }
        else {
            if(!$this->getCustomer()) {
                $this->customer = new Customer();
            }

            $this->customer->setAttributes($customer);
        }
    }
}